//ALL SECTION'S NAMES

var sectionNames = new Array(
    "hero_section",
    "quotes_section",
    "about_course",
    "who_benefits",
    "course_program",
    "subscribe_section",
    "about_author",
    "testimonials",
    "pricing",
    "faq"
);

//ALL SECTIONS SELECTORS TO SCROLL TO
var sections = new Array(
    $("#" + "hero_section"),
    $("#" + "quotes_section"),
    $("#" + "about_course"),
    $("#" + "who_benefits"),
    $("#" + "course_program"),
    $("#" + "subscribe_section"),
    $("#" + "about_author"),
    $("#" + "testimonials"),
    $("#" + "pricing"),
    $("#" + "faq")
);

//ALL SECTIONS SCROLL TO TOP HEIGHT
var sectionsScrollToTopHeight = new Array();

//ALL SECTIONS SCROLL TO BOTTOM HEIGHT
var sectionsScrollToBottomHeight = new Array();

for(let i = 0; i < sections.length; i++){
    sectionsScrollToTopHeight[i] = sections[i].offset().top;
    sectionsScrollToBottomHeight[i] = sectionsScrollToTopHeight[i] + sections[i].outerHeight();
}


function myScrollFunction(section) {

    $([document.documentElement, document.body]).animate({
        scrollTop: $("#" + section).offset().top
    }, 500);
}

$(".circle-link").click(function(event){

    $this = $(this);
    event.preventDefault();
    $(".circle").each(function(){
        $(this).removeClass("circle-filled");
    });
    $this.children().first().addClass("circle-filled");
});

$(window).on("resize scroll", function() {
    let windowTopPos = $(this).scrollTop();
    let windowBotPos = windowTopPos + $(this).height();
    let window_height = $(this).height();

    for(let i = 0; i < sections.length; i++){
        console.log(sectionsScrollToTopHeight[i], ((windowTopPos + windowBotPos) / 2) - window_height );
        console.log(sectionsScrollToBottomHeight[i], (windowTopPos + windowBotPos) / 2);
        if( sectionsScrollToTopHeight[i] < (((windowTopPos + windowBotPos) / 2) - window_height*0.15 ) && sectionsScrollToBottomHeight[i] > (((windowTopPos + windowBotPos) / 2) - window_height*0.15 )){
            $(".circle").each(function(){
                $(this).removeClass("circle-filled");
            });
            $("[data-section='" + sectionNames[i] +"']").children().first().addClass("circle-filled");
        }
    }
});

$("#open_popup").click(function(event){
    event.preventDefault();

    $("#pricing").css({
        "filter": "blur(2px)",
        "-webkit-filter": "blur(2px)",
        "-moz-filter": "blur(2px)",
        "-o-filter": "blur(2px)",
        "-ms-filter": "blur(2px)"
    });
    $("#faq").css({
        "filter": "blur(2px)",
        "-webkit-filter": "blur(2px)",
        "-moz-filter": "blur(2px)",
        "-o-filter": "blur(2px)",
        "-ms-filter": "blur(2px)"
    });
    $("body").css({
        "overflow" : "hidden"
    });
    $("#pop-content .popup_content").css({
        "opacity": "1",
        "transform": "translate(-50%, -50%) scale(1)",
        "visibility": "visible"
    });
    $("#pop-content").css({
        "opacity": "1",
        "visibility": "visible"
    });

});

$(".close_popup").click(function(event){
    event.preventDefault();

    $("#pricing").css({
        "filter": "",
        "-webkit-filter": "",
        "-moz-filter": "",
        "-o-filter": "",
        "-ms-filter": ""
    });
    $("#faq").css({
        "filter": "",
        "-webkit-filter": "",
        "-moz-filter": "",
        "-o-filter": "",
        "-ms-filter": ""
    });
    $("body").css({
        "overflow" : "visible"
    });
    $("#pop-content .popup_content").css({
        "opacity": "0",
        "transform": "translate(-50%, -50%) scale(.5)",
        "visibility": "hidden"
    });
    $("#pop-content").css({
        "opacity": "0",
        "visibility": "hidden"
    });
});

$("#open_popup1").click(function(event){
    event.preventDefault();

    $("#pricing").css({
        "filter": "blur(2px)",
        "-webkit-filter": "blur(2px)",
        "-moz-filter": "blur(2px)",
        "-o-filter": "blur(2px)",
        "-ms-filter": "blur(2px)"
    });
    $("#faq").css({
        "filter": "blur(2px)",
        "-webkit-filter": "blur(2px)",
        "-moz-filter": "blur(2px)",
        "-o-filter": "blur(2px)",
        "-ms-filter": "blur(2px)"
    });
    $("body").css({
        "overflow" : "hidden"
    });
    $("#pop-content1 .popup_content").css({
        "opacity": "1",
        "transform": "translate(-50%, -50%) scale(1)",
        "visibility": "visible"
    });
    $("#pop-content1").css({
        "opacity": "1",
        "visibility": "visible"
    });

});

$(".close_popup1").click(function(event){
    event.preventDefault();

    $("#pricing").css({
        "filter": "",
        "-webkit-filter": "",
        "-moz-filter": "",
        "-o-filter": "",
        "-ms-filter": ""
    });
    $("#faq").css({
        "filter": "",
        "-webkit-filter": "",
        "-moz-filter": "",
        "-o-filter": "",
        "-ms-filter": ""
    });
    $("body").css({
        "overflow" : "visible"
    });
    $("#pop-content1 .popup_content").css({
        "opacity": "0",
        "transform": "translate(-50%, -50%) scale(.5)",
        "visibility": "hidden"
    });
    $("#pop-content1").css({
        "opacity": "0",
        "visibility": "hidden"
    });
});